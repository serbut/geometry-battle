#include "font.h"

Font::Font()
{
    Load_font();
}

void Font::Load_font()
{
    sf::Image image;
    std::string path = "open_symbol_font";
    unsigned char ch;
    std::string buff;
    #ifdef __APPLE__
        image.loadFromFile(resourcePath() + "texture/" + path);
    #else
        image.loadFromFile("resources/font/" + path + ".png");
    #endif

    texture = 0;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA, image.getSize().x, image.getSize().y, GL_RGBA, GL_UNSIGNED_BYTE, image.getPixelsPtr());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    std::ifstream fin("resources/font/" + path + ".txt");
    fin >> num_symb;
    symb = new char32_t[num_symb];

    point_symb = new int*[num_symb];
    for(int i = 0; i < num_symb; i++)
        point_symb[i] = new int[2];

    size_symb = new int*[num_symb];
    for(int i = 0; i < num_symb; i++)
        size_symb[i] = new int[2];

//    std::string test = "абвгдеёжзийклмнопрстуфхцчшщьъыэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЬЪЫЭЮЯ";
//    for(int i = 0; i < test.length(); i+=2)
//    {
//        std::cout << "\n" << int (test[i]) << "  " << int (test[i + 1]);
//    }
//    std::cout << "\n\n";

    for(int i = 0; i < num_symb; i++)
    {
        fin >> buff;
        if (int(buff[0]) < 0)
        {
            symb[i] = convert_char(buff[0], buff[1]);
        }
        else
        {
            symb[i] = buff[0];
        }
        fin >> point_symb[i][0] >> point_symb[i][1];
        fin >> size_symb[i][0] >> size_symb[i][1];
    }
    fin.close();

    width = 672;
    height = 455;
}

void Font::Draw_symbol(char32_t ch, int x, int y)
{
    int symb_num = -1;
    for(int i = 0; i < num_symb; i++)
    {
        if (ch == symb[i])
            symb_num = i;
    }

    if (symb_num != -1)
    {
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
        glDisable(GL_DEPTH_TEST);
        glMatrixMode(GL_PROJECTION);
        glPushMatrix();
        glLoadIdentity();
        gluOrtho2D(0, WINDOW_WIDTH, 0, WINDOW_HEIGTH);
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glLoadIdentity();

        glBindTexture(GL_TEXTURE_2D, texture);
        glBegin(GL_QUADS);
        glTexCoord2f((1.0 / width) * point_symb[symb_num][0] , (1.0 / height) * point_symb[symb_num][1]);
        glVertex2f(x, y);
        glTexCoord2f((1.0 / width) * (point_symb[symb_num][0] + size_symb[symb_num][0]),
            (1.0 / height) * point_symb[symb_num][1]);
        glVertex2f(x + size_symb[symb_num][0], y);
        glTexCoord2f((1.0 / width) * (point_symb[symb_num][0] + size_symb[symb_num][0]),
            (1.0 / height) * (point_symb[symb_num][1] + size_symb[symb_num][1]));
        glVertex2f(x + size_symb[symb_num][0], y + size_symb[symb_num][1]);
        glTexCoord2f((1.0 / width) * point_symb[symb_num][0],
            (1.0 / height) * (point_symb[symb_num][1] + size_symb[symb_num][1]));
        glVertex2f(x, y + size_symb[symb_num][1]);
        glEnd();

        glMatrixMode(GL_MODELVIEW);
        glPopMatrix();
        glMatrixMode(GL_PROJECTION);
        glPopMatrix();
        glMatrixMode(GL_MODELVIEW);
        glEnable ( GL_DEPTH_TEST );
    }
}

void Font::Draw_symbol(char32_t ch, int x, int y, int w, int h)
{
    int symb_num = -1;
    for(int i = 0; i < num_symb; i++)
    {
        if (ch == symb[i])
            symb_num = i;
    }

    if (symb_num != -1)
    {
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
        glDisable(GL_DEPTH_TEST);
        glMatrixMode(GL_PROJECTION);
        glPushMatrix();
        glLoadIdentity();
        gluOrtho2D(0, WINDOW_WIDTH, 0, WINDOW_HEIGTH);
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glLoadIdentity();

        glBindTexture(GL_TEXTURE_2D, texture);
        glBegin(GL_QUADS);
        glTexCoord2f((1.0 / width) * point_symb[symb_num][0] , (1.0 / height) * point_symb[symb_num][1]);
        glVertex2f(x, y);
        glTexCoord2f((1.0 / width) * (point_symb[symb_num][0] + size_symb[symb_num][0]),
            (1.0 / height) * point_symb[symb_num][1]);
        glVertex2f(x + w, y);
        glTexCoord2f((1.0 / width) * (point_symb[symb_num][0] + size_symb[symb_num][0]),
            (1.0 / height) * (point_symb[symb_num][1] + size_symb[symb_num][1]));
        glVertex2f(x + w, y + h);
        glTexCoord2f((1.0 / width) * point_symb[symb_num][0],
            (1.0 / height) * (point_symb[symb_num][1] + size_symb[symb_num][1]));
        glVertex2f(x, y + h);
        glEnd();

        glMatrixMode(GL_MODELVIEW);
        glPopMatrix();
        glMatrixMode(GL_PROJECTION);
        glPopMatrix();
        glMatrixMode(GL_MODELVIEW);
        glEnable ( GL_DEPTH_TEST );
    }
}

int Font::Get_size_w(char32_t ch)
{
    int symb_num = -1;
    for(int i = 0; i < num_symb; i++)
    {
        if (ch == symb[i])
            symb_num = i;
    }

    return size_symb[symb_num][0];
}

int Font::Get_size_h(char32_t ch)
{
    int symb_num = -1;
    for(int i = 0; i < num_symb; i++)
    {
        if (ch == symb[i])
            symb_num = i;
    }

    return size_symb[symb_num][1];
}

Font::~Font()
{
    //dtor
}
