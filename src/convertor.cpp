#include "convertor.h"

#include <iostream>
const sf::Packet Convertor::getDataFromPlayer(Player* pl)
{


    sf::Packet packet;
    packet << pl->Get_center_mass().x
           << pl->Get_center_mass().y
           << pl->Get_center_mass().z
           << pl->Get_camera()->Get_angle_x()
           << pl->Get_camera()->Get_angle_y()
           << pl->Get_health();
//           << pl->Get_atackStatus()
    return packet;
}
void Convertor::setNewPlayerData(Player* pl, sf::Packet& packet)
{
    Point p(0, 0, 0);
    double angle_x = 0, angle_y = 0;
    int hp = 0;
//    bool as = 0;
    packet >> p.x
           >> p.y
           >> p.z
           >> angle_x
           >> angle_y
           >> hp;
//           >> as
    pl->Set_Player(p);
    pl->Get_camera()->Set_camera(angle_x, angle_y);
    pl->Set_health(hp);

}
