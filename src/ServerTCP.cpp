#include "ServerTCP.h"
#include <iostream>

ServerTCP::~ServerTCP()
{
    if(thread)
    {
        thread->wait();
        delete thread;
    }
}
void ServerTCP::StartConn(bool host)
{
    //sf::Thread* thread = 0;
    if(host){
        std::cout << "Host mode \n";
        InitServer();
    }
    else
    {
        std::cout << "Join mode \n";
        InitClient(); /// TODO Error CHeck here!!!!!
    }
    thread = new sf::Thread(&ServerTCP::Loop, this);
    thread->launch();

}
void ServerTCP::ServerEvent(const sf::Packet &pkt)
{
    globalMutex.lock();
    dataSend = pkt;
    globalMutex.unlock();
}
void ServerTCP::Loop()
{
    static sf::Packet oldData;
    while(!quit)
    {
        sf::Packet packetSend;
        globalMutex.lock();
        packetSend = dataSend;
        globalMutex.unlock();

        socket.send(packetSend);

        sf::Packet newData;

        socket.receive(newData);
        if(newData)
        {
//            if(oldData != newData)
//                    oldData = newData;
            RecivedData  = newData;
        }
    }
}
void ServerTCP::InitServer()
{
    sf::TcpListener listener;
    listener.listen(PORT);
    listener.accept(socket);
    std::cout << "New client connected: " << socket.getRemoteAddress() << std::endl;
}
bool ServerTCP::InitClient()
{
    if(socket.connect(recipient, PORT) == sf::Socket::Done)
    {
        std::cout << "Connected\n";
        return true;
    }
    return false;
}
void ServerTCP::Exit()
{
    quit = true;
}
