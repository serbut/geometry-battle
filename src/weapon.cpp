#include "weapon.h"

Weapon::Weapon(std::string new_wp)
{
    Create_weapon(new_wp);
}

void Weapon::Create_weapon(std::string new_wp)
{
    weapon = new Panel*[1];
    weapon[0] = new Panel(new_wp);
    std::ifstream fin("resources/weapon/" + new_wp + "/" + new_wp + ".txt");
    fin >> type >> damage >> radius_act >> max_cd >> max_patron;
    cd = 0;
    max_cd_reload = 60;
    cd_reload = 0;
    fin.close();
    num_patron = max_patron;
    name = new_wp;
}

void Weapon::Draw_weapon()
{
    weapon[0]->Draw_panel();

    if (cd > 0)
        cd--;

    if(cd_reload > 1)
        cd_reload--;

    if(cd_reload == 1)
    {
        num_patron = max_patron;
        cd_reload--;
    }

    char32_t ch;
    std::string num = int_to_str(num_patron);
    for(int i = 0; i < num.length(); i++)
    {
        ch = num[i];
        font.Draw_symbol(ch, 665 + i * 27, 150);
    }
    font.Draw_symbol('/', 665 + num.length() * 27, 147);
    int pos_max =  665 + num.length() * 27 + 22;
    num = int_to_str(max_patron);
    for(int i = 0; i < num.length(); i++)
    {
        ch = num[i];
        font.Draw_symbol(ch, pos_max + i * 27, 150);
    }
}

void Weapon::Use()
{
    cd = max_cd;
    if (num_patron > 0)
        num_patron--;

    if(num_patron == 0)
        cd_reload = max_cd_reload;
}

int Weapon::Get_cd()
{
    if(cd_reload != 0)
        return cd_reload;

    return cd;
}

std::string Weapon::Get_name()
{
    return name;
}

int Weapon::Get_max_cd()
{
    return max_cd;
}

int Weapon::Get_damage()
{
    return damage;
}

double Weapon::Get_radius_act()
{
    return radius_act;
}

int Weapon::Get_type()
{
    return type;
}

Weapon::~Weapon()
{
    //dtor
}
