#include "vector_dir.h"

Vector_dir::Vector_dir()
{
    x = 0;
    y = 0;
    z = 0;
}

Vector_dir::Vector_dir(double _x, double _y, double _z)
{
    x = _x;
    y = _y;
    z = _z;
}

Vector_dir::Vector_dir(const Vector_dir& vect)
{
    x = vect.x;
    y = vect.y;
    z = vect.z;
}

Vector_dir::Vector_dir(const Point& A, const Point& B)
{
    x = B.x - A.x;
    y = B.y - A.y;
    z = B.z - A.z;
}

Vector_dir::Vector_dir(const Vector_dir& vect, double k)
{
    x = vect.x / k;
    y = vect.y / k;
    z = vect.z / k;
}

Vector_dir::Vector_dir(const Vector_dir& vect1, const Vector_dir& vect2)
{
    x = vect1.x - vect2.y;
    y = vect1.y - vect2.y;
    z = vect1.z - vect2.z;
}

void Vector_dir::invers()
{
    x = -x;
    y = -y;
    z = -z;
}

void Vector_dir::Set_vector(double _x, double _y, double _z)
{
    x = _x;
    y = _y;
    z = _z;
}

void Vector_dir::Set_vector(const Vector_dir& vect, double k)
{
    x = vect.x / k;
    y = vect.y / k;
    z = vect.z / k;
}
