#include "camera.h"

Camera::Camera()
{
    PI = 3.141592;
}

Camera::Camera(double x_d, double y_d, double z_d)
{
    point_view.Set_point(x_d, y_d, z_d);
    PI = 3.141592;
}

void Camera::Set_camera(Vector_dir* orien)
{
    Point center;
    center.x = point_view.x + abs(orien->x) * tan(angleY / 180 * PI)
               - abs(orien->y) * sin(angleX / 180 * PI) - abs(orien->z) * cos(angleX / 180 * PI);
    center.y = point_view.y - abs(orien->x) * sin(angleX / 180 * PI)
               + abs(orien->y) * (tan(angleY / 180 * PI)) - abs(orien->z) * sin(angleX / 180 * PI);
    center.z = point_view.z - abs(orien->x) * cos(angleX / 180 * PI)
               - abs(orien->y) * cos(angleX / 180 * PI) + abs(orien->z) * tan(angleY / 180 * PI);

    gluLookAt(point_view.x, point_view.y, point_view.z, center.x, center.y, center.z, orien->x, orien->y, orien->z);
}

void Camera::CameraRotate(sf::RenderWindow & window, Vector_dir* orien)
{
    sf::Vector2i mouseXY = sf::Mouse::getPosition(window);

    int xt = window.getSize().x / 2;
    int yt = window.getSize().y / 2;

    angleX = angleX + (-orien->x + orien->y + orien->z) * (xt - mouseXY.x) / 4;
    angleY = angleY + (orien->x + orien->y + orien->z) * (yt - mouseXY.y) / 4;
  //  std::cout << "\nangle_X: " << angleX << "angle_Y: " << angleY;

    if (angleX < 0.0)
        angleX = 360.0;
    if (angleX > 360.0)
        angleX = 0;
    if (angleY < -89.0)
        angleY = -89.0;
    if (angleY > 89.0)
        angleY = 89.0;

    sf::Vector2i position;
    position.x = xt;
    position.y = yt;
    sf::Mouse::setPosition(position, window);
}

Point Camera::Get_point_view()
{
    return point_view;
}

void Camera::Set_view(Point v_p)
{
    Vector_dir cam_move(v_p, point_view);
    Vector_dir move_cam(cam_move, 20);

    point_view.x -= move_cam.x;
    point_view.y -= move_cam.y;
    point_view.z -= move_cam.z;
}

double Camera::Get_angle_x()
{
    return angleX;
}

double Camera::Get_angle_y()
{
    return angleY;
}

void Camera::Set_camera(double ang_x, double ang_y)
{
    if (ang_x > 360.0)
        angleX = ang_x - 360.0;
    else if (ang_x < 0.0)
        angleX = 360.0 + ang_x;
    else
        angleX = ang_x;
    angleY = ang_y;
}
