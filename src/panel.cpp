#include "panel.h"

Panel::Panel(std::string path)
{
    Create_panel(path);
}

void Panel::Create_panel(std::string path)
{
    std::ifstream fin("resources/texture/" + path + "/" + path + ".txt");
    fin >> x >> y >> w >> h >> max_state;
    texture = new GLuint[max_state];
    cur_state = 0;
    for(int i = 0; i < max_state; i++)
    {
        Create_texture(path, i);
    }
    fin.close();

}

void Panel::Create_texture(std::string path, int state)
{
    sf::Image image;
    #ifdef __APPLE__
        image.loadFromFile(resourcePath() + "texture/" + path);
    #else
        image.loadFromFile("resources/texture/" + path + "/" + path + "_" + int_to_str(state) + ".png");
    #endif

    texture[state] = 0;
    glGenTextures(1, &texture[state]);
    glBindTexture(GL_TEXTURE_2D, texture[state]);
    gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA, image.getSize().x, image.getSize().y, GL_RGBA, GL_UNSIGNED_BYTE, image.getPixelsPtr());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}

void Panel::Draw_panel()
{
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_DEPTH_TEST);
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluOrtho2D(0, WINDOW_WIDTH, 0, WINDOW_HEIGTH);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    glBindTexture(GL_TEXTURE_2D, texture[cur_state]); // биндим нужную нам текстуру
    glBegin(GL_QUADS);
    glTexCoord2f(0, 0);
    glVertex2f(x, y);
    glTexCoord2f(1, 0);
    glVertex2f(x + w, y);
    glTexCoord2f(1, 1);
    glVertex2f(x + w, y + h);
    glTexCoord2f(0, 1);
    glVertex2f(x, y + h);
    glEnd();

    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glEnable ( GL_DEPTH_TEST );
}

void Panel::Draw_panel_part(float** points, int num_p)
{
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_DEPTH_TEST);
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluOrtho2D(0, WINDOW_WIDTH, 0, WINDOW_HEIGTH);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    glBindTexture(GL_TEXTURE_2D, texture[cur_state]); // биндим нужную нам текстуру
    glBegin(GL_POLYGON);
    for(int i = 0; i < num_p; i++)
    {
        glTexCoord2f(points[i][0], points[i][1]);
        glVertex2f(x + points[i][0] * w, y + points[i][1] * h);
    }
    glEnd();

    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glEnable ( GL_DEPTH_TEST );
}


GLuint Panel::Get_texture(int _state)
{
    return texture[_state];
}

int Panel::Get_state()
{
    return cur_state;
}

void Panel::Set_state(int new_state)
{
    cur_state = new_state;
}

int Panel::Get_max_state()
{
    return max_state;
}

double Panel::Get_height()
{
    return h;
}

double Panel::Get_width()
{
    return w;
}

double Panel::Get_x()
{
    return x;
}

double Panel::Get_y()
{
    return y;
}

void Panel::Set_size(double _w, double _h)
{
    w = _w;
    h = _h;
}

void Panel::Set_pos(double _x, double _y)
{
    x = _x;
    y = _y;
}

Panel::~Panel()
{
    //dtor
}
