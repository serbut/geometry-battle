#include "../include/basics_include.hpp"
#include "../include/settings.hpp"
//#include "../include/basic_funcs.hpp"
#include "../include/world.h"
#include <clocale>
#include "ServerTCP.h"
#include "convertor.h"
#include "menu.h"

void menu();
void game(bool, bool);


int main()
{
    menu();
    return EXIT_SUCCESS;
}


void menu() {

    sf::RenderWindow window(
        sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGTH),
        "Main menu",
        sf::Style::Default,
        settings()
    );
	Menu menu(window.getSize().x, window.getSize().y);

	while (window.isOpen())
	{
		sf::Event event;

		while (window.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::KeyReleased:
				switch (event.key.code)
				{
				case sf::Keyboard::Up:
					menu.MoveUp();
					break;

				case sf::Keyboard::Down:
					menu.MoveDown();
					break;

				case sf::Keyboard::Return:
					switch (menu.GetPressedItem())
					{
					case 0:
						window.close();
						game(false, false);
						break;
					case 1:
                        window.close();
						game(true, true);
						break;
                    case 2:
                        window.close();
						game(true, false);
						break;
					case 3:
						window.close();
						break;

					}

					break;
				}

				break;
            case sf::Event::Closed:
				window.close();

				break;

			}
		}

		window.clear();

		menu.draw(window);

		window.display();
	}
}

void game(bool server, bool host) {
    if(server)  { // SERVER
        ServerTCP server("10.42.0.1", 5000);
    //  ServerTCP server("127.0.0.1", 5000);
        server.StartConn(host);
    }

    //    Create the main window

    sf::RenderWindow window(
        sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGTH),
        PROJECT_NAME,
        sf::Style::Default,
        settings()
    );
    window.setVerticalSyncEnabled(true);
    sf::Vector2i v1(400, 100);
    window.setPosition(v1);


    window.setActive();

    // Enable Z-buffer read and write
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
    glClearDepth(1.f);
    glClearColor(0.f, 0.f, 0.f, 0.f);

    // Setup a perspective projection
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(90.f, ((float) WINDOW_WIDTH)/ WINDOW_HEIGTH, .01f, 300.0f);


    // Bind our texture
    glEnable(GL_TEXTURE_2D);
    //  glBindTexture(GL_TEXTURE_2D, texture);
    //  glColor4f(1.f, 1.f, 1.f, 1.f);

    // Create a clock for measuring the time elapsed

    sf::Clock clock;

    World world;
    Convertor conv;
    Menu menu(window.getSize().x, window.getSize().y);

    // Start game loop
    while (window.isOpen())
    {
        // Process events
        sf::Event event;

/*        if(server)  {
            server.ServerEvent(conv.getDataFromPlayer(world.Get_player(0)));
            conv.setNewPlayerData(world.Get_player(1), server.RecivedData);
        } // SERVER
*/
        while (window.pollEvent(event))
        {
            // Close window : exit
            if (event.type == sf::Event::Closed)
                window.close();

            // Escape key : exit
            if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::Escape))
                window.close();
        }

        bool winOnFocus = window.hasFocus();

        if (winOnFocus)
            world.Get_player(0)->Get_camera()->CameraRotate(window, world.Get_player(0)->Get_orien());

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glClear(GL_DEPTH_BUFFER_BIT);

        double size = 1.0;
        double size_2 = 2.0;
        world.Draw_world(size_2, winOnFocus);

        window.display();
    }
    //   glDeleteTextures(1, &texture);

}



