#include "hero_spells.h"

Hero_spells::Hero_spells()
{
    spells = new Spell_hero*[3];
    max_spells = 0;
}

void Hero_spells::Create_spell(std::string spell)
{
    spells[max_spells] = new Spell_hero(spell);
    max_spells++;
}

int Hero_spells::Get_max_spells()
{
    return max_spells;
}

Spell_hero* Hero_spells::Get_spell(int num)
{
    return spells[num];
}
//void Hero_spells::Delete_spells()
//{
//
//
//}
//
//void Hero_spells::Check_spells()
//{
//
//
//}

Hero_spells::~Hero_spells()
{
    //dtor
}
