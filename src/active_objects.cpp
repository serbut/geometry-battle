#include "active_objects.h"

Active_objects::Active_objects()
{
    objects = new Active_object*[100];
    max_objects = 0;
    num_objects = 0;
}

void Active_objects::Create_object(Point p, double ang_x, double ang_y, Vector_dir* vect, Spell_hero* sp)
{
    objects[max_objects] = new Active_object(p, ang_x, ang_y, vect, sp);
    max_objects++;
    std::cout << "\n max_objects = "<< max_objects;
}

void Active_objects::Create_object(Point p, double ang_x, double ang_y, Vector_dir* vect, Weapon* wp)
{
    objects[max_objects] = new Active_object(p, ang_x, ang_y, vect, wp);
    max_objects++;
    std::cout << "\n max_objects = "<< max_objects;
}

void Active_objects::Draw_objects()
{
    for(int i = 0; i < max_objects; i++)
    {
        if(objects[i]->Move_object())
            objects[i]->Draw_object();
        else
            Delete_object(i);
    }
}

int Active_objects::Check_objects(int*** map_world, int w, int h, int d, Point pl_loc)
{
    for(int i = 0; i < max_objects; i++)
    {
        if(objects[i]->Check_object_map(map_world, w, h, d))
        {
            std::cout << "\n map_true";
            Delete_object(i);
        }
        else if (objects[i]->Check_object_pl(pl_loc))
            return i;
    }
    return -1;
}

void Active_objects::Delete_object(int num)
{
    delete objects[num];
    for(int i = num + 1; i < max_objects; i++)
        objects[i - 1] = objects[i];
    max_objects--;
}

Active_object* Active_objects::Get_object(int num)
{
    return objects[num];
}

Active_objects::~Active_objects()
{
    //dtor
}

