#include "object.h"

Object::Object(std::string str, double _size)
{
    Create_object(str, _size);
};

void Object::Create_object(std::string str, double _size)
{
    size = _size;
#ifdef __APPLE__
    crate[0] = LoadTexture(resourcePath() + str + "/front.jpg");
    crate[1] = LoadTexture(resourcePath() + str + "/back.jpg");
    crate[2] = LoadTexture(resourcePath() + str + "/left.jpg");
    crate[3] = LoadTexture(resourcePath() + str + "/right.jpg");
    crate[4] = LoadTexture(resourcePath() + str + "/top.jpg");
    crate[5] = LoadTexture(resourcePath() + str + "/bottom.jpg");
#else
    crate[0] = LoadTexture("resources/" + str + "/front.jpg");
    crate[1] = LoadTexture("resources/" + str + "/back.jpg");
    crate[2] = LoadTexture("resources/" + str + "/left.jpg");
    crate[3] = LoadTexture("resources/" + str + "/right.jpg");
    crate[4] = LoadTexture("resources/" + str + "/bottom.jpg");
    crate[5] = LoadTexture("resources/" + str + "/top.jpg");
#endif

    crate_des = new int**[6];
    for(int i = 0; i < 6; i++)
        crate_des[i] = new int*[6];

    for(int i = 0; i < 6; i++)
        for(int j = 0; j < 6; j++)
            crate_des[i][j] = new int[8];

    crate_des[5][0][0] = 1; crate_des[5][0][1] = 1; crate_des[5][0][2] = 1; crate_des[5][0][3] = 0;
    crate_des[5][0][4] = 0; crate_des[5][0][5] = 0; crate_des[5][0][6] = 0; crate_des[5][0][7] = 1;

    crate_des[5][1][0] = 0; crate_des[5][1][1] = 0; crate_des[5][1][2] = 0; crate_des[5][1][3] = 1;
    crate_des[5][1][4] = 1; crate_des[5][1][5] = 1; crate_des[5][1][6] = 1; crate_des[5][1][7] = 0;

    crate_des[5][2][0] = 0; crate_des[5][2][1] = 1; crate_des[5][2][2] = 1; crate_des[5][2][3] = 1;
    crate_des[5][2][4] = 1; crate_des[5][2][5] = 0; crate_des[5][2][6] = 0; crate_des[5][2][7] = 0;

    crate_des[5][3][0] = 0; crate_des[5][3][1] = 1; crate_des[5][3][2] = 1; crate_des[5][3][3] = 1;
    crate_des[5][3][4] = 1; crate_des[5][3][5] = 0; crate_des[5][3][6] = 0; crate_des[5][3][7] = 0;

    crate_des[5][4][0] = 0; crate_des[5][4][1] = 0; crate_des[5][4][2] = 0; crate_des[5][4][3] = 1;
    crate_des[5][4][4] = 1; crate_des[5][4][5] = 1; crate_des[5][4][6] = 1; crate_des[5][4][7] = 0;

    crate_des[5][5][0] = 0; crate_des[5][5][1] = 0; crate_des[5][5][2] = 0; crate_des[5][5][3] = 1;
    crate_des[5][5][4] = 1; crate_des[5][5][5] = 1; crate_des[5][5][6] = 1; crate_des[5][5][7] = 0;

}

GLuint Object::LoadTexture(sf::String name)
{
    sf::Image image;
    image.loadFromFile(name);
    image.flipVertically();

    GLuint texture=0;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA, image.getSize().x, image.getSize().y, GL_RGBA, GL_UNSIGNED_BYTE, image.getPixelsPtr());
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    return texture;
}

void Object::Draw_object()
{
    glBindTexture(GL_TEXTURE_2D, crate[0]);
    glBegin(GL_QUADS);
    glTexCoord2f(0, 0);
    glVertex3f(-size, -size, -size);
    glTexCoord2f(1, 0);
    glVertex3f( size, -size, -size);
    glTexCoord2f(1, 1);
    glVertex3f( size,  size, -size);
    glTexCoord2f(0, 1);
    glVertex3f(-size,  size, -size);
    glEnd();

    glBindTexture(GL_TEXTURE_2D, crate[1]);
    glBegin(GL_QUADS);
    glTexCoord2f(0, 0);
    glVertex3f(size, -size, size);
    glTexCoord2f(1, 0);
    glVertex3f(-size,  -size, size);
    glTexCoord2f(1, 1);
    glVertex3f( -size,  size, size);
    glTexCoord2f(0, 1);
    glVertex3f( size, size, size);
    glEnd();

    glBindTexture(GL_TEXTURE_2D, crate[2]);
    glBegin(GL_QUADS);
    glTexCoord2f(0, 0);
    glVertex3f(-size, -size, -size);
    glTexCoord2f(0, 1);
    glVertex3f(-size,  size, -size);
    glTexCoord2f(1, 1);
    glVertex3f(-size,  size,  size);
    glTexCoord2f(1, 0);
    glVertex3f(-size, -size,  size);
    glEnd();

    glBindTexture(GL_TEXTURE_2D, crate[3]);
    glBegin(GL_QUADS);
    glTexCoord2f(0, 0);
    glVertex3f(size, -size, -size);
    glTexCoord2f(0, 1);
    glVertex3f(size,  size, -size);
    glTexCoord2f(1, 1);
    glVertex3f(size,  size,  size);
    glTexCoord2f(1, 0);
    glVertex3f(size, -size,  size);
    glEnd();

    glBindTexture(GL_TEXTURE_2D, crate[4]);
    glBegin(GL_QUADS);
    glTexCoord2f(0, 1);
    glVertex3f(-size, -size,  size);
    glTexCoord2f(0, 0);
    glVertex3f(-size, -size, -size);
    glTexCoord2f(1, 0);
    glVertex3f( size, -size, -size);
    glTexCoord2f(1, 1);
    glVertex3f( size, -size,  size);
    glEnd();

    glBindTexture(GL_TEXTURE_2D, crate[5]);
    glBegin(GL_QUADS);
    glTexCoord2f(0, 1);
    glVertex3f(-size, size,  size);
    glTexCoord2f(0, 0);
    glVertex3f(-size, size, -size);
    glTexCoord2f(1, 0);
    glVertex3f( size, size, -size);
    glTexCoord2f(1, 1);
    glVertex3f( size, size,  size);
    glEnd();
}

void Object::Draw_object(double _w, double _h, double _d, Vector_dir* orien)
{
    int* crate_num = new int[6];

    if (orien->x != 0)
    {   _w = _h; _h = _d; }
    if (orien->z != 0)
    {   _d = _h; _h = _w; }

  //  std::cout << "\n" << orien->x << "  " << orien->y << "  " << orien->z;

    if (orien->x == 1)
    {    crate_num[0] = 0; crate_num[1] = 1; crate_num[2] = 4; crate_num[3] = 5; crate_num[4] = 2; crate_num[5] = 3; }
    if (orien->x == -1)
    {    crate_num[0] = 0; crate_num[1] = 1; crate_num[2] = 5; crate_num[3] = 4; crate_num[4] = 2; crate_num[5] = 3; }
    if (orien->y == 1)
    {    crate_num[0] = 0; crate_num[1] = 1; crate_num[2] = 2; crate_num[3] = 3; crate_num[4] = 4; crate_num[5] = 5; }
    if (orien->y == -1)
    {    crate_num[0] = 0; crate_num[1] = 1; crate_num[2] = 2; crate_num[3] = 3; crate_num[4] = 5; crate_num[5] = 4; }
    if (orien->z == 1)
    {    crate_num[0] = 4; crate_num[1] = 5; crate_num[2] = 2; crate_num[3] = 3; crate_num[4] = 0; crate_num[5] = 1; }
    if (orien->z == -1)
    {    crate_num[0] = 5; crate_num[1] = 4; crate_num[2] = 2; crate_num[3] = 3; crate_num[4] = 0; crate_num[5] = 1; }


    glBindTexture(GL_TEXTURE_2D, crate[crate_num[0]]);
    glBegin(GL_QUADS);
    glTexCoord2f(1, 0);
    glVertex3f(-_w, -_h, -_d);
    glTexCoord2f(0, 0);
    glVertex3f( _w, -_h, -_d);
    glTexCoord2f(0, 1);
    glVertex3f( _w,  _h, -_d);
    glTexCoord2f(1, 1);
    glVertex3f(-_w,  _h, -_d);
    glEnd();

    glBindTexture(GL_TEXTURE_2D, crate[crate_num[1]]);
    glBegin(GL_QUADS);
    glTexCoord2f(0, 0);
    glVertex3f( _w, -_h, _d);
    glTexCoord2f(0, 1);
    glVertex3f(-_w, -_h, _d);
    glTexCoord2f(1, 1);
    glVertex3f(-_w,  _h, _d);
    glTexCoord2f(1, 0);
    glVertex3f( _w,  _h, _d);
    glEnd();

    glBindTexture(GL_TEXTURE_2D, crate[crate_num[2]]);
    glBegin(GL_QUADS);
    glTexCoord2f(0, 1);
    glVertex3f(-_w, -_h, -_d);
    glTexCoord2f(1, 1);
    glVertex3f(-_w,  _h, -_d);
    glTexCoord2f(1, 0);
    glVertex3f(-_w,  _h,  _d);
    glTexCoord2f(0, 0);
    glVertex3f(-_w, -_h,  _d);
    glEnd();

    glBindTexture(GL_TEXTURE_2D, crate[crate_num[3]]);
    glBegin(GL_QUADS);
    glTexCoord2f(0, 1);
    glVertex3f( _w, -_h, -_d);
    glTexCoord2f(1, 1);
    glVertex3f( _w,  _h, -_d);
    glTexCoord2f(1, 0);
    glVertex3f( _w,  _h,  _d);
    glTexCoord2f(0, 0);
    glVertex3f( _w, -_h,  _d);
    glEnd();

    glBindTexture(GL_TEXTURE_2D, crate[crate_num[4]]);
    glBegin(GL_QUADS);
    glTexCoord2f(0, 0);
    glVertex3f(-_w, -_h,  _d);
    glTexCoord2f(0, 1);
    glVertex3f(-_w, -_h, -_d);
    glTexCoord2f(1, 1);
    glVertex3f( _w, -_h, -_d);
    glTexCoord2f(1, 0);
    glVertex3f( _w, -_h,  _d);
    glEnd();

    glBindTexture(GL_TEXTURE_2D, crate[crate_num[5]]);
    glBegin(GL_QUADS);
    glTexCoord2f(0, 0);
    glVertex3f(-_w,  _h,  _d);
    glTexCoord2f(0, 1);
    glVertex3f(-_w,  _h, -_d);
    glTexCoord2f(1, 1);
    glVertex3f( _w,  _h, -_d);
    glTexCoord2f(1, 0);
    glVertex3f( _w,  _h,  _d);
    glEnd();

    delete crate_num;
}

void Object::Draw_object_rot(double angle, Vector_dir* orien)
{
    if(orien->x == -1 || orien->y == 1 || orien->z == 1)
        glRotated(angle, orien->x, orien->y, orien->z);
    else
        glRotated(360 - angle, orien->x, orien->y, orien->z);
}

Object::~Object()
{
    for(int i = 0; i < 6; i++)
        glDeleteTextures(1, &crate[i]);
};
