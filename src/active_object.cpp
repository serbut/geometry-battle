#include "active_object.h"

Active_object::Active_object(Point loc, double ang_x, double ang_y, Vector_dir* orien, Spell_hero* sp)
{
    double size, speed;
    std::ifstream fin("resources/" + sp->Get_name() + "_box/" + sp->Get_name() + ".txt");
        fin >> size >> speed;
    fin.close();
    time_exist = 0;
    max_time_exist = 100;
    damage = sp->Get_damage();
    radius_act = sp->Get_radius_act();
    double PI = 3.14159;
    object_models = new Object*[1];
    object_models[0] = new Object(sp->Get_name() + "_box", size);
    location.x = loc.x; location.y = loc.y; location.z = loc.z;

    if(orien->x != 0)
    {
        vect_move.y = -sin(ang_x / 180 * PI) * speed;
        vect_move.x = -sin(-ang_y / 180 * PI) * speed;
        vect_move.z = -cos(ang_x / 180 * PI) * speed;
    }
    if(orien->y != 0)
    {
        vect_move.y = -sin(-ang_y / 180 * PI) * speed;
        vect_move.x = -sin(ang_x / 180 * PI) * speed;
        vect_move.z = -cos(ang_x / 180 * PI) * speed;
    }
    if(orien->z != 0)
    {
        vect_move.y = -sin(ang_x / 180 * PI) * speed;
        vect_move.x = -cos(ang_x / 180 * PI) * speed;
        vect_move.z = -sin(-ang_y / 180 * PI) * speed;
    }
    std::cout << "\ncreate_spell";
}

Active_object::Active_object(Point loc, double ang_x, double ang_y, Vector_dir* orien, Weapon* wp)
{
    double size, speed;
    std::ifstream fin("resources/" + wp->Get_name() + "_box/" + wp->Get_name() + ".txt");
        fin >> size >> speed;
    fin.close();
    time_exist = 0;
    max_time_exist = 100;
    damage = wp->Get_damage();
    radius_act = wp->Get_radius_act();
    double PI = 3.14159;
    object_models = new Object*[1];
    object_models[0] = new Object(wp->Get_name() + "_box", size);
    location.x = loc.x; location.y = loc.y; location.z = loc.z;

    if(orien->x != 0)
    {
        vect_move.y = -sin(ang_x / 180 * PI) * speed;
        vect_move.x = -sin(-ang_y / 180 * PI) * speed;
        vect_move.z = -cos(ang_x / 180 * PI) * speed;
    }
    if(orien->y != 0)
    {
        vect_move.y = -sin(-ang_y / 180 * PI) * speed;
        vect_move.x = -sin(ang_x / 180 * PI) * speed;
        vect_move.z = -cos(ang_x / 180 * PI) * speed;
    }
    if(orien->z != 0)
    {
        vect_move.y = -sin(ang_x / 180 * PI) * speed;
        vect_move.x = -cos(ang_x / 180 * PI) * speed;
        vect_move.z = -sin(-ang_y / 180 * PI) * speed;
    }
    std::cout << "\ncreate_spell";
}


void Active_object::Draw_object()
{
    glTranslated(location.x, location.y, location.z);
    object_models[0]->Draw_object();
    glTranslated(-location.x, -location.y, -location.z);
}

int Active_object::Move_object()
{
    time_exist++;
    if(time_exist < max_time_exist)
    {
        location.x += vect_move.x;
        location.y += vect_move.y;
        location.z += vect_move.z;
        return 1;
    }
    return 0;
}

int Active_object::Check_object_map(int*** map_world, int w, int h, int d)
{
    if(location.x / 2 >= 0 && location.y / 2 >= 0 && location.z / 2 >= 0 &&
        location.x / 2 < w && location.y / 2 < h && location.z / 2 < d)
    {
        if(map_world[int(location.x / 2)][int(location.y / 2)][int(location.z / 2)] == 1)
        {
            return 1;
        }
    }
    return 0;
}

int Active_object::Check_object_pl(Point pl_loc)
{
    if(distance(location, pl_loc) < 2)
        return 1;
    return 0;
}

int Active_object::Get_damage()
{
    return damage;
}

double Active_object::Get_radius_act()
{
    return radius_act;
}

Point Active_object::Get_location()
{
    return location;
}

Active_object::~Active_object()
{
    for(int i = 0; i < 1; i++)
        delete object_models[i];
    delete [] object_models;
}
