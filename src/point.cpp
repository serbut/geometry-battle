#include "point.h"

Point::Point()
{
    x = 0;
    y = 0;
    z = 0;
}

Point::Point(double _x, double _y, double _z)
{
    x = _x;
    y = _y;
    z = _z;
}

Point::Point(const Point& point)
{
    x = point.x;
    y = point.y;
    z = point.z;
}

Point::Point(const Point& point, const Vector_dir& vect)
{
    x = point.x - vect.x;
    y = point.y - vect.y;
    z = point.z - vect.z;
}

void Point::Set_point(double _x, double _y, double _z)
{
    x = _x;
    y = _y;
    z = _z;
}

void Point::Set_point(const Point& point)
{
    x = point.x;
    y = point.y;
    z = point.z;
}

void Point::Set_point(const Point& point, const Vector_dir& vect)
{
    x = point.x - vect.x;
    y = point.y - vect.y;
    z = point.z - vect.z;
}

