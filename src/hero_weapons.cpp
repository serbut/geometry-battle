#include "hero_weapons.h"

Hero_weapons::Hero_weapons()
{
    weapons = new Weapon*[3];
    act_weapon = 0;
    max_weapons = 0;
}

void Hero_weapons::Create_weapon(std::string new_wp)
{
    weapons[max_weapons] = new Weapon(new_wp);
    max_weapons++;
}

int Hero_weapons::Get_max_weapons()
{
    return max_weapons;
}

int Hero_weapons::Get_num_act_weapon()
{
    return act_weapon;
}

Weapon* Hero_weapons::Get_act_weapon()
{
    return weapons[act_weapon];
}

Weapon* Hero_weapons::Get_weapon(int num)
{
    return weapons[num];
}

//void Hero_spells::Delete_spells()
//{
//
//
//}
//
//void Hero_spells::Check_spells()
//{
//
//
//}
Hero_weapons::~Hero_weapons()
{
    //dtor
}
