#include "spell_hero.h"

Spell_hero::Spell_hero(std::string new_sp)
{
    Create_spell(new_sp);
}

void Spell_hero::Create_spell(std::string new_sp)
{
    spell = new Panel*[2];
    spell[0] = new Panel(new_sp);
    spell[1] = new Panel("cooldown");
    std::ifstream fin("resources/spell/" + new_sp + "/" + new_sp + ".txt");
    fin >> type >> damage >> radius_act >> max_cd;
    fin.close();
    cd = 0;
    active = 0;
    name = new_sp;
}

void Spell_hero::Draw_spell()
{
    spell[0]->Draw_panel();

    if (cd != 0)
    {
        int num_ps = cd / (max_cd / 8) + 3;
        int n_p = 2;
        float rad = spell[1]->Get_height() / 2;
        float angle = 6.283 - 6.283 * cd / max_cd;
        float x = (spell[1]->Get_width() / 2 + rad * sin(angle)) / spell[1]->Get_width();
        float y = (spell[1]->Get_height() / 2 + rad * cos(angle)) / spell[1]->Get_height();
    //    std::cout << "\n points_num:  " << num_ps << "  " << x << "  " << y << "  " << angle << "  " << rad * sin(angle);

        float** points = new float*[num_ps];
        for(int i = 0; i < num_ps; i++)
            points[i] = new float[2];

        points[0][0] = 0.5; points[0][1] = 0.5;
        points[1][0] = x; points[1][1] = y;

        if (num_ps - 2 > 7)
            { points[n_p][0] = 1; points[n_p][1] = 1; n_p++; }
        if (num_ps - 2 > 6)
            { points[n_p][0] = 1; points[n_p][1] = 0.5; n_p++; }
        if (num_ps - 2 > 5)
            { points[n_p][0] = 1; points[n_p][1] = 0; n_p++; }
        if (num_ps - 2 > 4)
            { points[n_p][0] = 0.5; points[n_p][1] = 0; n_p++; }
        if (num_ps - 2 > 3)
            { points[n_p][0] = 0; points[n_p][1] = 0; n_p++; }
        if (num_ps - 2 > 2)
            { points[n_p][0] = 0; points[n_p][1] = 0.5; n_p++; }
        if (num_ps - 2 > 1)
            { points[n_p][0] = 0; points[n_p][1] = 1; n_p++; }
        if (num_ps - 2 > 0)
            { points[n_p][0] = 0.5; points[n_p][1] = 1; n_p++; }

        spell[1]->Draw_panel_part(points, num_ps);

        cd--;
    }
}

void Spell_hero::Use()
{
    cd = max_cd;
}

std::string Spell_hero::Get_name()
{
    return name;
}

int Spell_hero::Get_max_cd()
{
    return max_cd;
}

int Spell_hero::Get_cd()
{
    return cd;
}

int Spell_hero::Get_damage()
{
    return damage;
}

double Spell_hero::Get_radius_act()
{
    return radius_act;
}

int Spell_hero::Get_active()
{
    return active;
}

void Spell_hero::Set_active(int _active)
{
    active = _active;
}

Spell_hero::~Spell_hero()
{
    //dtor
}
