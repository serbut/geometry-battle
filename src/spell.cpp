#include "spell.h"

Spell::Spell(Point loc, double ang_x, double ang_y, Vector_dir* orien)
{
    time_exist = 0;
    max_time_exist = 100;
    damage = 1;
    radius_act = 0.3;
    double PI = 3.14159;
    spell_models = new Object*[1];
    spell_models[0] = new Object("fireball_box", 0.3);
    location.x = loc.x; location.y = loc.y; location.z = loc.z;

    if(orien->x != 0)
    {
        vect_move.y = -sin(ang_x / 180 * PI) * 0.2;
        vect_move.x = -sin(-ang_y / 180 * PI) * 0.2;
        vect_move.z = -cos(ang_x / 180 * PI) * 0.2;
    }
    if(orien->y != 0)
    {
        vect_move.y = -sin(-ang_y / 180 * PI) * 0.2;
        vect_move.x = -sin(ang_x / 180 * PI) * 0.2;
        vect_move.z = -cos(ang_x / 180 * PI) * 0.2;
    }
    if(orien->z != 0)
    {
        vect_move.y = -sin(ang_x / 180 * PI) * 0.2;
        vect_move.x = -cos(ang_x / 180 * PI) * 0.2;
        vect_move.z = -sin(-ang_y / 180 * PI) * 0.2;
    }

}

void Spell::Draw_spell()
{
    glTranslated(location.x, location.y, location.z);
    spell_models[0]->Draw_object();
    glTranslated(-location.x, -location.y, -location.z);
}

int Spell::Move_spell()
{
    time_exist++;
    if(time_exist < max_time_exist)
    {
        location.x += vect_move.x;
        location.y += vect_move.y;
        location.z += vect_move.z;
        return 1;
    }
    return 0;
}

int Spell::Check_spell_map(int*** map_world, int size_world)
{
    if(location.x / 2 >= 0 && location.y / 2 >= 0 && location.z / 2 >= 0 &&
        location.x / 2 < size_world && location.y / 2 < size_world && location.z / 2 < size_world)
    {
        if(map_world[int(location.x / 2)][int(location.y / 2)][int(location.z / 2)] == 1)
        {
            return 1;
        }
    }
    return 0;
}

int Spell::Check_spell_pl(Point pl_loc)
{
    if(distance(location, pl_loc) < 2)
        return 1;
    return 0;
}

int Spell::Get_damage()
{
    return damage;
}

double Spell::Get_radius_act()
{
    return radius_act;
}

Point Spell::Get_location()
{
    return location;
}

Spell::~Spell()
{
    for(int i = 0; i < 1; i++)
        delete spell_models[i];
    delete [] spell_models;
}
