#include "basic_funcs.hpp"

 double distance(Point point1, Point point2) {
    return sqrt(pow(point1.x - point2.x, 2.0) + pow(point1.y - point2.y, 2.0) + pow(point1.z - point2.z, 2.0));
}

double distance(Vector_dir vect){
    return sqrt(pow(vect.x, 2.0) + pow(vect.y, 2.0) + pow(vect.z, 2.0));
}

std::string int_to_str(int num)
{
    std::string str;
    char ch;

    if (num == 0)
    {
        str += "0";
        return str;
    }

    while(num > 0)
    {
        str += char(num % 10 + 48);
        num /= 10;
    }

    for(int i = 0; i < str.length() / 2; i++)
    {
        ch = str[i];
        str[i] = str[str.length() - 1 - i];
        str[str.length() - 1 - i] = ch;
    }

    return str;
}

sf::ContextSettings settings() {
    sf::ContextSettings settings;
    settings.depthBits = SETTINGS_DEPTH_BITS;
    settings.stencilBits = SETTINGS_STENCIL_BITS;
    settings.majorVersion = SETTINGS_MAJOR_VERSION;
    settings.minorVersion = SETTINGS_MINOR_VERSION;
    return settings;
}

int str_to_int(std::string str)
{
    int num = 0;

    for(int i = str.length() - 1; i > -1; i--)
    {
        num = num * 10 + int(str[i]) - 48;
    }

    return num;
}

char32_t convert_char (int ch_code, int ch_code2)
{
    if (ch_code == -48 && ch_code2 == -112) return 'А';
    if (ch_code == -48 && ch_code2 == -111) return 'Б';
    if (ch_code == -48 && ch_code2 == -110) return 'В';
    if (ch_code == -48 && ch_code2 == -109) return 'Г';
    if (ch_code == -48 && ch_code2 == -108) return 'Д';
    if (ch_code == -48 && ch_code2 == -107) return 'Е';
    if (ch_code == -48 && ch_code2 == -127) return 'Ё';
    if (ch_code == -48 && ch_code2 == -106) return 'Ж';
    if (ch_code == -48 && ch_code2 == -105) return 'З';
    if (ch_code == -48 && ch_code2 == -104) return 'И';
    if (ch_code == -48 && ch_code2 == -103) return 'Й';
    if (ch_code == -48 && ch_code2 == -102) return 'К';
    if (ch_code == -48 && ch_code2 == -101) return 'Л';
    if (ch_code == -48 && ch_code2 == -100) return 'М';
    if (ch_code == -48 && ch_code2 == -99) return 'Н';
    if (ch_code == -48 && ch_code2 == -98) return 'О';
    if (ch_code == -48 && ch_code2 == -97) return 'П';
    if (ch_code == -48 && ch_code2 == -96) return 'Р';
    if (ch_code == -48 && ch_code2 == -95) return 'С';
    if (ch_code == -48 && ch_code2 == -94) return 'Т';
    if (ch_code == -48 && ch_code2 == -93) return 'У';
    if (ch_code == -48 && ch_code2 == -92) return 'Ф';
    if (ch_code == -48 && ch_code2 == -91) return 'Х';
    if (ch_code == -48 && ch_code2 == -90) return 'Ц';
    if (ch_code == -48 && ch_code2 == -89) return 'Ч';
    if (ch_code == -48 && ch_code2 == -88) return 'Ш';
    if (ch_code == -48 && ch_code2 == -87) return 'Щ';
    if (ch_code == -48 && ch_code2 == -86) return 'Ь';
    if (ch_code == -48 && ch_code2 == -85) return 'Ъ';
    if (ch_code == -48 && ch_code2 == -84) return 'Ы';
    if (ch_code == -48 && ch_code2 == -83) return 'Э';
    if (ch_code == -48 && ch_code2 == -82) return 'Ю';
    if (ch_code == -48 && ch_code2 == -81) return 'Я';

    if (ch_code == -48 && ch_code2 == -80) return 'а';
    if (ch_code == -48 && ch_code2 == -79) return 'б';
    if (ch_code == -48 && ch_code2 == -78) return 'в';
    if (ch_code == -48 && ch_code2 == -77) return 'г';
    if (ch_code == -48 && ch_code2 == -76) return 'д';
    if (ch_code == -48 && ch_code2 == -75) return 'е';
    if (ch_code == -47 && ch_code2 == -111) return 'ё';
    if (ch_code == -48 && ch_code2 == -74) return 'ж';
    if (ch_code == -48 && ch_code2 == -73) return 'з';
    if (ch_code == -48 && ch_code2 == -72) return 'и';
    if (ch_code == -48 && ch_code2 == -71) return 'й';
    if (ch_code == -48 && ch_code2 == -70) return 'к';
    if (ch_code == -48 && ch_code2 == -69) return 'л';
    if (ch_code == -48 && ch_code2 == -68) return 'м';
    if (ch_code == -48 && ch_code2 == -67) return 'н';
    if (ch_code == -48 && ch_code2 == -66) return 'о';
    if (ch_code == -48 && ch_code2 == -65) return 'п';
    if (ch_code == -47 && ch_code2 == -128) return 'р';
    if (ch_code == -47 && ch_code2 == -127) return 'с';
    if (ch_code == -47 && ch_code2 == -126) return 'т';
    if (ch_code == -47 && ch_code2 == -125) return 'у';
    if (ch_code == -47 && ch_code2 == -124) return 'ф';
    if (ch_code == -47 && ch_code2 == -123) return 'х';
    if (ch_code == -47 && ch_code2 == -122) return 'ц';
    if (ch_code == -47 && ch_code2 == -121) return 'ч';
    if (ch_code == -47 && ch_code2 == -120) return 'ш';
    if (ch_code == -47 && ch_code2 == -119) return 'щ';
    if (ch_code == -47 && ch_code2 == -116) return 'ь';
    if (ch_code == -47 && ch_code2 == -118) return 'ъ';
    if (ch_code == -47 && ch_code2 == -117) return 'ы';
    if (ch_code == -47 && ch_code2 == -115) return 'э';
    if (ch_code == -47 && ch_code2 == -114) return 'ю';
    if (ch_code == -47 && ch_code2 == -113) return 'я';

    return ch_code;
}

