#include "player.h"

Player::Player(int _type, int _allies)
{
    Create_player(_type, _allies);
};

void Player::Create_player(int _type, int _allies)
{
    type = _type;
    allies = _allies;
    orien.Set_vector(0, 1, 0);
    center_mass.Set_point(14, 18, 16);
    flag_fly = 0;

    std::string path;
    std::string path2 = "";
    std::string buff;

    if (type == 1)
        path = "mage";

    if (allies == 2)
        path2 += "_en";

    std::ifstream fin("resources/player/" + path + "/" + path + ".txt");
    fin >> max_health;
    health = max_health;
    fin.close();

    height = 2.0;
    h = 0.7; w = 0.4; d = 0.4;

    camera = new Camera*[1];
    camera[0] = new Camera(center_mass.x, center_mass.y + height, center_mass.z);
    feet.Set_point(center_mass.x, center_mass.y - height, center_mass.z);

    player_models = new Object*[1];
    player_models[0] = new Object(path + "_box", 0.6);

    std::ifstream fin2("resources/player/" + path + "/spells.txt");
    while(fin2 >> buff)
    {
        spells.Create_spell(buff);
    }
    fin2.close();

    std::ifstream fin3("resources/player/" + path + "/weapons.txt");
    while(fin3 >> buff)
    {
        weapons.Create_weapon(buff);
    }
    fin3.close();

    num_health_panel = max_health / 10;
    health_panel = new Panel*[num_health_panel];
    health_panel[0] = new Panel("heart" + path2);
    for(int i = 1; i < num_health_panel; i++)
    {
        health_panel[i] = new Panel("heart" + path2);
        if (allies == 1)
            health_panel[i]->Set_pos(health_panel[i - 1]->Get_x() + health_panel[i - 1]->Get_width() + 2, health_panel[i - 1]->Get_y());
        else
            health_panel[i]->Set_pos(health_panel[i - 1]->Get_x() - health_panel[i - 1]->Get_width() - 2, health_panel[i - 1]->Get_y());
    }

    im_hero = new Panel*[1];
    im_hero[0] = new Panel(path + path2);

    flag_spell = -1;
    flag_weapon = -1;

    std::ifstream fin4("resources/player/player_" + int_to_str(allies) + "/player.txt");
    fin4 >> name;
    fin4.close();
}

void Player::Draw_player()
{
    glTranslated(center_mass.x, center_mass.y, center_mass.z);
    player_models[0]->Draw_object_rot(camera[0]->Get_angle_x(), &orien);
  //  std::cout << "\n" << orien.x << "  " << orien.y << "  " << orien.z;
    player_models[0]->Draw_object(w, h, d, &orien);
    glTranslated(-center_mass.x, -center_mass.y, -center_mass.z);
}

void Player::Draw_panel()
{
    for(int i = 0; i < num_health_panel; i++)
    {
         health_panel[i]->Draw_panel();
    }
    im_hero[0]->Draw_panel();

    if (allies == 1)
    {
        for(int i = 0; i < spells.Get_max_spells(); i++)
            spells.Get_spell(i)->Draw_spell();

        weapons.Get_act_weapon()->Draw_weapon();
    }

    char32_t ch;
    int pos = 0;
    for(int i = 0; i < name.length(); i++)
    {
        if (int(name[i]) < 0)
        {
            ch = convert_char(name[i], name[i + 1]);
            i++;
        }
        else
            ch = convert_char(name[i], name[i]);

        if (allies == 1)
            font.Draw_symbol(ch, 110 + pos, 550);
        else
            font.Draw_symbol(ch, 440 + pos, 550);

        pos += font.Get_size_w(ch) + 2;
    }
}

void Player::Move_player(int*** map_world, int _w, int _h, int _d)
{
    Vector_dir vect_move;
    double speed = 0.075;
    int w_w = _w;
    int h_w = _h;
    int d_w = _d;
    double PI = 3.141592;
    double angle_x = camera[0]->Get_angle_x();
  //  double angle_y = camera[0]->Get_angle_y();

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
    {
        vect_move.y = -sin(angle_x / 180 * PI) * speed * (abs(orien.x) + abs(orien.z));
        vect_move.x = -sin(angle_x / 180 * PI) * speed * abs(orien.y) - cos(angle_x / 180 * PI) * speed * abs(orien.z) ;
        vect_move.z = -cos(angle_x / 180 * PI) * speed * abs(orien.x) - cos(angle_x / 180 * PI) * speed * abs(orien.y);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
    {
        vect_move.y = sin(angle_x / 180 * PI) * speed * abs(orien.x) + sin(angle_x / 180 * PI) * speed * abs(orien.z);
        vect_move.x = sin(angle_x / 180 * PI) * speed * abs(orien.y) + cos(angle_x / 180 * PI) * speed * abs(orien.z);
        vect_move.z = cos(angle_x / 180 * PI) * speed * abs(orien.x) + cos(angle_x / 180 * PI) * speed * abs(orien.y);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
    {
        vect_move.y = sin((angle_x - orien.x * 90) / 180 * PI) * speed * abs(orien.x)
                      + sin((angle_x + orien.z * 90) / 180 * PI) * speed * abs(orien.z);
        vect_move.x = sin((angle_x + orien.y * 90) / 180 * PI) * speed * abs(orien.y)
                      + cos((angle_x + orien.z * 90) / 180 * PI) * speed * abs(orien.z);
        vect_move.z = cos((angle_x - orien.x * 90) / 180 * PI) * speed * abs(orien.x)
                      + cos((angle_x + orien.y * 90) / 180 * PI) * speed * abs(orien.y);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
    {
        vect_move.y = sin((angle_x + orien.x * 90) / 180 * PI) * speed * abs(orien.x)
                      + sin((angle_x - orien.z * 90) / 180 * PI) * speed * abs(orien.z);
        vect_move.x = sin((angle_x - orien.y * 90) / 180 * PI) * speed * abs(orien.y)
                      + cos((angle_x - orien.z * 90) / 180 * PI) * speed * abs(orien.z);
        vect_move.z = cos((angle_x + orien.x * 90) / 180 * PI) * speed * abs(orien.x)
                      + cos((angle_x - orien.y * 90) / 180 * PI) * speed * abs(orien.y);
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && flag_fly == 0)
    {
        vect_move.x = orien.x * 4;
        vect_move.y = orien.y * 4;
        vect_move.z = orien.z * 4;
        flag_fly = 1;
    }

    vect_move.invers();
    Point pos_cm(center_mass, vect_move);
    if(vect_move.x != 0 || vect_move.y != 0 || vect_move.z != 0)
    {
        if(pos_cm.x / 2 >= 0 && pos_cm.y / 2 >= 0 && pos_cm.z / 2 >= 0 && pos_cm.x / 2 < w_w && pos_cm.y / 2 < h_w && pos_cm.z / 2 < d_w)
        {
            if(map_world[int(pos_cm.x / 2)][int(pos_cm.y / 2)][int(pos_cm.z / 2)] != 1)
            {
                Move(vect_move);
                // camera[0]->Set_point_view(vect_move);
            }
        }
    }
}

void Player::Gravity(Vector_dir cm_gr, int*** map_world, int _w, int _h, int _d)
{
    Point point_view(camera[0]->Get_point_view());
    Point ground(center_mass, cm_gr);

    Vector_dir pl_move(cm_gr, 50);
    Vector_dir cm_gr_n(Check_orien(cm_gr));

    Vector_dir cm_view_pos(cm_gr_n, distance(cm_gr_n) / height);
    Point v_p(center_mass, cm_view_pos);
    camera[0]->Set_view(v_p);

   // Vector_dir cm_feet_pos(cm_gr_n, distance(cm_gr_n) / height);
  //  cm_feet_pos.invers();
   // Point c_p(center_mass, cm_feet_pos);
   // feet.Set_point(c_p);

//    Vector_dir pl_move;
//    Point v_p;
//    std::cout << "\n" << p_gr.x << "  " << p_gr.y << "  " << p_gr.z;
//    std::cout << "\n" << ground.x << "  " << ground.y << "  " << ground.z;
//    if (p_gr.x != ground.x || p_gr.y != ground.y || p_gr.z != ground.z)
//    {
//        pl_move.Set_vector(cm_gr, 100);
//        Vector_dir cm_gr_n(Check_orien(cm_gr));
//
//        Vector_dir cm_view_pos(cm_gr_n, distance(cm_gr_n) / height);
//        v_p.Set_point(center_mass, cm_view_pos);
//        camera[0]->Set_view(v_p);
//
//        Vector_dir cm_feet_pos(cm_gr_n, distance(cm_gr_n) / height);
//        cm_feet_pos.invers();
//        Point c_p(center_mass, cm_feet_pos);
//        feet.Set_point(c_p);
//    }
//    else
//    {
//        Vector_dir feet_gr(feet, p_gr);
//        feet_gr.invers();
//        pl_move.Set_vector(feet_gr, 100);
//        Vector_dir feet_gr_n(Check_orien(feet_gr));
//
//        Vector_dir cm_view_pos(feet_gr_n, distance(feet_gr_n) / (2 * height));
//        v_p.Set_point(feet, cm_view_pos);
//        camera[0]->Set_view(v_p);
//
//        Vector_dir cm_feet_pos(feet_gr_n, distance(feet_gr_n) / height);
//        Point c_p(feet, cm_feet_pos);
//        center_mass.Set_point(c_p);
//    }
//    p_gr.Set_point(ground);
////    if (flag_fly == 0)
////        p_gr.Set_point(ground);
//    std::cout << "\n" << ground.x << "  " << ground.y << "  " << ground.z;

//    if(map_world[int(feet.x / 2)][int(feet.y / 2)][int(feet.z / 2)] == 1)
//    {
////        pl_move.invers();
////        Move(pl_move);
//    }
//    else
//    {
    Point pos_c(center_mass, pl_move);
    if(pos_c.x / 2 >= 0 && pos_c.y / 2 >= 0 && pos_c.z / 2 >= 0 && pos_c.x / 2 < _w && pos_c.y / 2 < _h && pos_c.z / 2 < _d)
    {
        if(map_world[int(pos_c.x / 2)][int(pos_c.y / 2)][int(pos_c.z / 2)] != 1)
        {
            flag_fly = 1;
            Move(pl_move);
        }
        else
            flag_fly = 0;
    }
//    }

    Point v_p_2(v_p, pl_move);
    camera[0]->Set_view(v_p_2);
}

Vector_dir Player::Check_orien(Vector_dir vect)
{
    if(abs(vect.x) > abs(vect.y))
        if(abs(vect.x) > abs(vect.z))
        {
            vect.y = 0;
            vect.z = 0;
        }
        else
        {
            vect.x = 0;
            vect.y = 0;
        }
    else if (abs(vect.y) > abs(vect.z))
    {
        vect.x = 0;
        vect.z = 0;
    }
    else
    {
        vect.x = 0;
        vect.y = 0;
    }
    Change_orien(vect.x, vect.y, vect.z);
    vect.invers();

    return vect;
}

void Player::Change_orien(double _x, double _y, double _z)
{
    if (_x < 0)
        _x = -1;
    if (_x > 0)
        _x = 1;
    if (_y < 0)
        _y = -1;
    if (_y > 0)
        _y = 1;
    if (_z < 0)
        _z = -1;
    if (_z > 0)
        _z = 1;
    if (orien.x != _x || orien.y != _y || orien.z != _z)
    {
        camera[0]->Set_camera(camera[0]->Get_angle_x(), 0);
        if ((orien.y == 1 && _z == -1) || (orien.y == -1 && _z == 1))
            camera[0]->Set_camera(360 - camera[0]->Get_angle_x() + 90, camera[0]->Get_angle_y());
        if ((orien.z == -1 && _y == -1) || (orien.z == 1 && _y == 1))
            camera[0]->Set_camera(camera[0]->Get_angle_x() + 90, camera[0]->Get_angle_y());
        if ((orien.z == 1 && _y == -1) || (orien.z == -1 && _y == 1))
            camera[0]->Set_camera(90 - camera[0]->Get_angle_x(), camera[0]->Get_angle_y());
        if ((orien.y == 1 && _z == 1) || (orien.y == -1 && _z == -1))
            camera[0]->Set_camera(camera[0]->Get_angle_x() - 90, camera[0]->Get_angle_y());
        if ((orien.y == 1 && _x == 1) || (orien.y == -1 && _x == -1) || (orien.x == 1 && _y == 1) || (orien.x == -1 && _y == -1))
            camera[0]->Set_camera(360 - camera[0]->Get_angle_x(), camera[0]->Get_angle_y());
        if ((orien.x == 1 && _z == 1) || (orien.x == -1 && _z == -1))
            camera[0]->Set_camera(360 - camera[0]->Get_angle_x() - 180, camera[0]->Get_angle_y());
        if ((orien.z == 1 && _x == 1) || (orien.z == -1 && _x == -1))
            camera[0]->Set_camera(180 - camera[0]->Get_angle_x(), camera[0]->Get_angle_y());
    }
//  std::cout << "\norien: x = " << _x << " y = " << _y << " z = " << _z;
    orien.Set_vector(_x, _y, _z);
}

int Player::Check_attack()
{
    if (spells.Get_max_spells() > 0)
    {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::E) && spells.Get_spell(0)->Get_cd() == 0)
        {
            spells.Get_spell(0)->Use();
            flag_spell = 0;
        }
    }
    if (spells.Get_max_spells() > 1)
    {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::R) && spells.Get_spell(1)->Get_cd() == 0)
        {
            spells.Get_spell(1)->Use();
            flag_spell = 1;
        }
    }
    if (spells.Get_max_spells() > 2)
    {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::C) && spells.Get_spell(2)->Get_cd() == 0)
        {
            spells.Get_spell(2)->Use();
            flag_spell = 2;
        }
    }
    if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && weapons.Get_weapon(0)->Get_cd() == 0)
    {
        weapons.Get_act_weapon()->Use();
        flag_weapon = weapons.Get_num_act_weapon();
    }

    if(flag_spell != -1 || flag_weapon != -1)
        return 1;

    return 0;
}

Spell_hero* Player::Get_spell()
{
    return spells.Get_spell(flag_spell);
}

Weapon* Player::Get_weapon()
{
    return weapons.Get_weapon(flag_weapon);
}

int Player::Get_flag_spell()
{
    return flag_spell;
}

int Player::Get_flag_weapon()
{
    return flag_weapon;
}

void Player::Reset_flag()
{
    flag_spell = -1;
    flag_weapon = -1;
}

void Player::Take_effect(Active_object* act_obj)
{
    health -= act_obj->Get_damage();

    num_health_panel = 0;
    int cur_health = health;
    for(int i = 0; cur_health >= 10; i++)
    {
        cur_health -= 10;
        health_panel[i]->Set_state(0);
        num_health_panel++;
    }

    if(cur_health > 0)
    {
        if (cur_health >= 8 && cur_health < 10)
            health_panel[num_health_panel]->Set_state(0);
        else if (cur_health >= 6 && cur_health < 8)
            health_panel[num_health_panel]->Set_state(1);
        else if (cur_health >= 3 && cur_health < 6)
            health_panel[num_health_panel]->Set_state(2);
        else
            health_panel[num_health_panel]->Set_state(3);

        num_health_panel++;
    }
}


Vector_dir* Player::Get_orien()
{
    return &orien;
}

void Player::Move(Vector_dir vect_move)
{
    center_mass.x -= vect_move.x;
    center_mass.y -= vect_move.y;
    center_mass.z -= vect_move.z;
}

void Player::Set_Player(const Point &p)
{
    center_mass.x = p.x;
    center_mass.y = p.y;
    center_mass.z = p.z;
}

int Player::Get_health()
{
    return health;
}

int Player::Get_max_health()
{
    return max_health;
}

void Player::Set_health(int _health)
{
    health = _health;
}

Camera* Player::Get_camera()
{
    return camera[0];
}

Point Player::Get_center_mass()
{
    return center_mass;
}

Object* Player::Get_player_model()
{
    return player_models[0];
}
