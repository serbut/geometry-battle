#include "world.h"

World::World()
{
    Generate_world();
    players = new Player*[2];
    players[0] = new Player(1, 1);
    players[1] = new Player(1, 2);
};

void World::Generate_world()
{
    std::cout << "gen_world";
    int buff;

    std::ifstream fin("resources/map/map_1/map");
    fin >> w >> h >> d;

    map_world = new int**[w];
    for(int i = 0; i < w; i++)
    {
        map_world[i] = new int*[h];
        for(int j = 0; j < h; j++)
            map_world[i][j] = new int[d];
    }

    for(int i = 0; i < w; i++)
    {
        fin >> buff;
        for(int j = 0; j < h; j++)
            for(int k = 0; k < d; k++)
                fin >> map_world[i][j][k];
    }
    fin.close();
//    w = 30;
//    h = 30;
//    d = 30;
//    map_world = new int**[w];
//    for(int i = 0; i < w; i++)
//    {
//        map_world[i] = new int*[h];
//        for(int j = 0; j < h; j++)
//            map_world[i][j] = new int[d];
//    }
//
//    for(int i = 0; i < w; i++)
//        for(int j = 0; j < h; j++)
//            for(int k = 0; k < d; k++)
//                map_world[i][j][k] = 0;
//
////   ---------------------------------------Куб с 2-мя дырками
//    for(int i = 2; i < 9; i++)
//        for(int j = 2; j < 9; j++)
//            for(int k = 2; k < 9; k++)
//                map_world[i][j][k] = 1;
//
//    for(int i = 2; i < 9; i++)
//        for(int j = 4; j < 7; j++)
//            for(int k = 4; k < 7; k++)
//                map_world[i][j][k] = 0;
//
//    for(int i = 4; i < 7; i++)
//        for(int j = 4; j < 7; j++)
//            for(int k = 2; k < 9; k++)
//                map_world[i][j][k] = 0;

    objects = new Object*[2];
    objects[0] = new Object("ground_box", 1.0);

    skyboxes = new Object*[1];
    skyboxes[0] = new Object("sky_box", 100);

    time = 0;

//    for(int i = 0; i < size_world; i++)
//    {
//        for(int j = 0; j < size_world; j++)
//        {
//            for(int k = 0; k < size_world; k++)
//                std::cout << map_world[i][j][k] << " ";
//            std::cout << "\n";
//        }
//        std::cout << "\n";
//    }
}

void World::Draw_world(double size, bool winOnFocus = true)
{
    time += 1;

    if (winOnFocus)
    {
        players[0]->Move_player(map_world, w, h, d);
        World_Gravity();
        players[0]->Get_camera()->Set_camera(players[0]->Get_orien());
        if(players[0]->Check_attack())
        {
            if(players[0]->Get_flag_spell() != -1)
            {
                if(players[0]->Get_flag_spell() == 0)
                {
                    obj_spells.Create_object(players[0]->Get_camera()->Get_point_view(), players[0]->Get_camera()->Get_angle_x(),
                        players[0]->Get_camera()->Get_angle_y(), players[0]->Get_orien(), players[0]->Get_spell());
                }
            }
            if(players[0]->Get_flag_weapon() != -1)
            {
                if(players[0]->Get_flag_weapon() == 0)
                {
                    obj_spells.Create_object(players[0]->Get_camera()->Get_point_view(), players[0]->Get_camera()->Get_angle_x(),
                        players[0]->Get_camera()->Get_angle_y(), players[0]->Get_orien(), players[0]->Get_weapon());
                }
            }
            players[0]->Reset_flag();
        }
    }
    for(int i = 0; i < w; i++)
    {
        for(int j = 0; j < h; j++)
        {
            for(int k = 0; k < d; k++)
            {
                if(map_world[i][j][k] == 1)
                {
                    glTranslated(i * size + size / 2, j * size + size / 2, k * size + size / 2);
                    objects[0]->Draw_object();
                    glTranslated(-i * size - size / 2, -j * size - size / 2, -k * size - size / 2);
                }
            }
        }
    }
    glTranslated(players[0]->Get_center_mass().x, players[0]->Get_center_mass().y, players[0]->Get_center_mass().z);
    skyboxes[0]->Draw_object();
    glTranslated(-players[0]->Get_center_mass().x, -players[0]->Get_center_mass().y, -players[0]->Get_center_mass().z);

    players[1]->Draw_player();
    players[1]->Draw_panel();

    int check_sp = obj_spells.Check_objects(map_world, w, h, d, players[1]->Get_center_mass());
    if (check_sp != -1)
    {
        players[1]->Take_effect(obj_spells.Get_object(check_sp));
        obj_spells.Delete_object(check_sp);
        std::cout << "\nplayer_1  " << players[0]->Get_health() << "/" << players[0]->Get_max_health()
            << "  player_2  " << players[1]->Get_health() << "/" << players[1]->Get_max_health();
    }
    obj_spells.Draw_objects();

   // players[0]->Draw_player();
    players[0]->Draw_panel();
 }

void World::World_Gravity()
{
    double r_min = 0;
    double r = 0;
    Vector_dir vect;
    Point c_m(players[0]->Get_center_mass());
    Point gr;
    for(int i = 0; i < w; i++)
    {
        for(int j = 0; j < h; j++)
        {
            for(int k = 0; k < d; k++)
            {
                if(map_world[i][j][k] == 1)
                {
                    gr.Set_point(i * 2 + 1, j * 2 + 1, k * 2 + 1);
                    r = distance(c_m, gr);
                    if(r_min > r || r_min == 0)
                    {
                        r_min = r;
                        vect.Set_vector(c_m.x - gr.x, c_m.y - gr.y, c_m.z - gr.z);
                    }
                }
            }
        }
    }
  //  std::cout << "\n" << r_min;
    players[0]->Gravity(vect, map_world, w, h, d);
}

int*** World::Get_map_world()
{
    return map_world;
}

Player* World::Get_player(int num_player)
{
    return players[num_player];
}

World::~World() {};

