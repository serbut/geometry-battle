#ifndef HERO_SPELLS_H
#define HERO_SPELLS_H
#include "spell_hero.h"

class Hero_spells
{
    public:
        Hero_spells();
        void Create_spell(std::string);
  //      void Check_spells();
 //       void Delete_spells();
        int Get_max_spells();
        Spell_hero* Get_spell(int);
        virtual ~Hero_spells();

    private:
        Spell_hero** spells;
        int max_spells;
};

#endif // HERO_SPELLS_H
