#ifndef PANEL_H
#define PANEL_H
#include <string>
#include <fstream>
#include "basics_include.hpp"
#include "basic_funcs.hpp"
#include "settings.hpp"
#include "font.h"

class Panel
{
    public:
        Panel(std::string);
        void Create_panel(std::string);
        void Create_texture(std::string, int);
        void Draw_panel();
        void Draw_panel_part(float**, int);
        GLuint Get_texture(int);
        double Get_height();
        double Get_width();
        double Get_x();
        double Get_y();
        void Set_size(double, double);
        void Set_pos(double, double);
        int Get_state();
        void Set_state(int);
        int Get_max_state();

        virtual ~Panel();

    private:
        int cur_state;
        int max_state;
        double x, y;
        double h, w;
        GLuint* texture;
};

#endif // PANEL_H
