#ifndef CONVERTOR_H
#define CONVERTOR_H

#include "player.h"
#include <SFML/Network.hpp>
#include "point.h"


class Convertor
{
    public:
        Convertor(){};
        virtual ~Convertor(){};
        const sf::Packet getDataFromPlayer(Player* pl);
        void setNewPlayerData(Player* pl, sf::Packet& pkt);

    private:
};

#endif // CONVERTOR_H
