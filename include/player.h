#ifndef PLAYER_H
#define PLAYER_H
#include "basics_include.hpp"
#include "camera.h"
#include "object.h"
#include "hero_spells.h"
#include "hero_weapons.h"
#include "active_object.h"
#include "panel.h"
#include "font.h"

double distance(Vector_dir);
double distance(Point, Point);

class Player
{
public:
    Player(int, int);
    void Create_player(int, int);
    void Draw_player();
    void Draw_panel();
    int Get_health();
    int Get_max_health();
    void Set_health(int);
    void Move_player(int *** map_world, int, int, int);
    void Gravity(Vector_dir cm_gr, int *** map_world, int, int, int);
    Vector_dir Check_orien(Vector_dir vect);
    void Change_orien(double _x, double _y, double _z);
    Vector_dir* Get_orien();
    void Move(Vector_dir vect_move);
    void Set_Player(const Point &p);
    void Take_effect(Active_object*);
    int Check_attack();
    int Get_flag_spell();
    int Get_flag_weapon();
    int Get_type();
    int Get_allies();
    Spell_hero* Get_spell();
    Weapon* Get_weapon();
    void Reset_flag();
    Camera* Get_camera();
    Point Get_center_mass();
    Object* Get_player_model();
    ~Player() {};

private:
    int type;
    Point center_mass;
    Point feet;
    Point p_gr;
    double height;
    double h, w, d;
    Camera** camera;
    Object** player_models;
    Hero_spells spells;
    Hero_weapons weapons;
    Panel** health_panel;
    Panel** im_hero;
    Font font;
    int flag_spell;
    int flag_weapon;

    Vector_dir orien;
    int flag_fly;
    int health;
    int max_health;
    int num_health_panel;
    int allies;
    std::string name;

};
#endif // PLAYER_H
