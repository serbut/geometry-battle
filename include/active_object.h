#ifndef ACTIVE_OBJECT_H
#define ACTIVE_OBJECT_H
#include <string>
#include <fstream>
#include "spell_hero.h"
#include "weapon.h"
#include "basic_funcs.hpp"
#include "object.h"
#include "vector_dir.h"
#include "point.h"

class Active_object
{
    public:
        Active_object(Point, double, double, Vector_dir*, Spell_hero*);
        Active_object(Point, double, double, Vector_dir*, Weapon*);
        void Draw_object();
        int Move_object();
        int Get_damage();
        int Check_time();
        double Get_radius_act();

        int Check_object_map(int***, int, int, int);
        int Check_object_pl(Point);
        Point Get_location();

        virtual ~Active_object();

    private:
        Object** object_models;
        Point location;
        Vector_dir vect_move;
        int time_exist;
        int max_time_exist;
        int damage;
        double radius_act;
};

#endif // ACTIVE_OBJECT_H
