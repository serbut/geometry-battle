#ifndef BASICS_INCLUDE
#define BASICS_INCLUDE


#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#ifdef __APPLE__
#include "ResourcePath.hpp"
#include <GLUT/glut.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/glu.h>
#include <GL/glut.h>
#endif
#include <cmath>
#include "cstring"
#include <iostream>
#include <memory>

#endif // BASICS_INCLUDE
