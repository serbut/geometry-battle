#ifndef POINT_H
#define POINT_H
#include "vector_dir.h"

class Vector_dir;

class Point{
public:
    double x;
    double y;
    double z;

    Point();
    Point(double _x, double _y, double _z);
    Point(const Point& point);
    Point(const Point& point, const Vector_dir& vect);

    void Set_point(double, double, double);
    void Set_point(const Point&);
    void Set_point(const Point& point, const Vector_dir& vect);

    bool Empty()
    {
        return false;
    }

    bool equals(const Point &rhs) const
    {
        return (x == rhs.x && y == rhs.y && z == rhs.z);

    }
    bool operator!=(const Point &rhs)
    {
        return !equals(rhs);
    }
};

#endif // POINT_H
