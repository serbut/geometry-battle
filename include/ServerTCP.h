#ifndef SERVERTCP_H
#define SERVERTCP_H

#include <SFML/Network.hpp>
#include "point.h"

class ServerTCP
{
    public:
        ServerTCP(sf::IpAddress ip = "127.0.0.1",
                  unsigned short port = 5000):
                  recipient(ip),
                  PORT(port),
                  thread(0),
                  quit(false){};
        virtual ~ServerTCP();
        void StartConn(bool);
        void ServerEvent(const sf::Packet &p);
        void Exit();
        sf::Packet RecivedData;

    private:
        const unsigned short PORT;
        const sf::IpAddress recipient;
        sf::Packet dataSend;
        sf::TcpSocket socket;
        sf::Mutex globalMutex;

        sf::Thread* thread;
        bool quit;
        void Loop();
        void InitServer();
        bool InitClient();
};


#endif // SERVERTCP_H


/*
1) Создать объект сервера
2) Выбрать тип соедиени
3) Запустить соединени





*/
