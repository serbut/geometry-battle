#ifndef WEAPON_H
#define WEAPON_H
#include "panel.h"

class Weapon
{
    public:
        Weapon(std::string);
        void Create_weapon(std::string);
        void Draw_weapon();
        void Use();
        std::string Get_name();
        int Get_max_cd();
        int Get_cd();
        int Get_damage();
        double Get_radius_act();
        int Get_type();

        virtual ~Weapon();

    private:
        int num_patron;
        int max_patron;
        Panel** weapon;
        Font font;
        int type;
        int damage;
        double radius_act;
        int max_cd;
        int cd;
        int cd_reload;
        int max_cd_reload;
        std::string name;
};

#endif // WEAPON_H
