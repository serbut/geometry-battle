#ifndef WORLD_H
#define WORLD_H
#include "player.h"
#include "active_objects.h"

class World
{
public:
    World();
    void Generate_world();
    void Draw_world(double, bool);
    void World_Gravity();
    int*** Get_map_world();
    Player* Get_player(int);
    ~World();

private:
    Object** objects;
    Object** skyboxes;
    Active_objects obj_spells;
    int*** map_world;
    Player** players;
    int w, h, d;
    int size_world;
    double time;
};

#endif // WORLD_H
