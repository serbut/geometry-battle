#ifndef OBJECT_H
#define OBJECT_H
#include "basics_include.hpp"
#include "settings.hpp"
#include <iostream>

class Object
{
public:
    Object(std::string, double);
    void Create_object(std::string, double);
    GLuint LoadTexture(sf::String);
    void Draw_object();
    void Draw_object(double, double, double, Vector_dir*);
    void Draw_object_rot(double, Vector_dir*);
    ~Object();

private:
    int h, w, d;
    GLuint crate[6];
    int*** crate_des;
    double size;
};


#endif // OBJECT_H
