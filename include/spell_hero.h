#ifndef SPELL_HERO_H
#define SPELL_HERO_H
#include "panel.h"

class Spell_hero
{
    public:
        Spell_hero(std::string);
        void Create_spell(std::string);
        void Draw_spell();
        int Get_damage();
        int Get_cd();
        int Get_max_cd();
        std::string Get_name();
        void Use();
        void Set_active(int);
        int Get_active();
        double Get_radius_act();
        virtual ~Spell_hero();

    private:
        Panel** spell;
        int type;
        int cd;
        int max_cd;
        int damage;
        int active;
        double radius_act;
        std::string name;
};

#endif // SPELL_HERO_H
