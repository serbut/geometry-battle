#ifndef ACTIVE_OBJECTS_H
#define ACTIVE_OBJECTS_H
#include "active_object.h"

class Active_objects
{
    public:
        Active_objects();
        void Create_object(Point, double, double, Vector_dir*, Spell_hero*);
        void Create_object(Point, double, double, Vector_dir*, Weapon*);
        void Draw_objects();
        int Check_objects(int***, int, int, int, Point);
        void Delete_object(int);
        Active_object* Get_object(int);
        virtual ~Active_objects();

    private:
        Active_object** objects;
        int max_objects;
        int num_objects;
};

#endif // ACTIVE_OBJECTS_H
