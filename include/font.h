#ifndef FONT_H
#define FONT_H
#include <string>
#include <fstream>
#include "basic_funcs.hpp"
#include "basics_include.hpp"
#include "settings.hpp"

class Font
{
    public:
        Font();
        void Load_font();
        void Draw_symbol(char32_t, int, int);
        void Draw_symbol(char32_t, int, int, int, int);
        int Get_size_w(char32_t);
        int Get_size_h(char32_t);
        virtual ~Font();

    private:
        int** point_symb;
        int** size_symb;
        int num_symb;
        char32_t* symb;
        int width;
        int height;
        GLuint texture;
};

#endif // FONT_H
