#ifndef HERO_WEAPONS_H
#define HERO_WEAPONS_H
#include "weapon.h"

class Hero_weapons
{
    public:
        Hero_weapons();
        void Create_weapon(std::string);
  //      void Check_spells();
 //       void Delete_spells();
        int Get_num_act_weapon();
        int Get_max_weapons();
        Weapon* Get_act_weapon();
        Weapon* Get_weapon(int);
        virtual ~Hero_weapons();

    private:
        Weapon** weapons;
        int max_weapons;
        int act_weapon;
};

#endif // HERO_WEAPONS_H
