#ifndef BASIC_FUNCS_H
#define BASIC_FUNCS_H

#include <string>
#include "settings.hpp"
#include "basics_include.hpp"

double distance(Point, Point);
double distance(Vector_dir);
std::string int_to_str(int);
int str_to_int(std::string);
sf::ContextSettings settings();
char32_t convert_char(int, int);

#endif // BASIC_FUNCS_H
