#ifndef CAMERA_H
#define CAMERA_H
#include "basics_include.hpp"
#include "settings.hpp"
#include <iostream>

class Camera
{
public:
    Camera();
    Camera(double x_d, double y_d, double z_d);
    void Set_camera(Vector_dir* orien);
    void Set_camera(double, double);
    void CameraRotate(sf::RenderWindow & window, Vector_dir* orien);
    Point Get_point_view();
    void Set_view(Point v_p);
    double Get_angle_x();
    double Get_angle_y();
    ~Camera() {};

private:
    double angleX, angleY;
    Point point_view;
    double PI;
};

#endif // CAMERA_H
