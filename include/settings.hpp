#ifndef SETTINGS_HPP_INCLUDED
#define SETTINGS_HPP_INCLUDED
#include <cmath>
#include "point.h"
#include "vector_dir.h"

//project name
#define PROJECT_NAME "Geometry Battle"

// define setting parameters
#define SETTINGS_DEPTH_BITS 24
#define SETTINGS_STENCIL_BITS 8
#define SETTINGS_MAJOR_VERSION 3
#define SETTINGS_MINOR_VERSION 0

//define window size
#define WINDOW_HEIGTH 600
#define WINDOW_WIDTH 800

//resources paths

#endif // SETTINGS_HPP_INCLUDED
