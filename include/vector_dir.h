#ifndef VECTOR_DIR_H
#define VECTOR_DIR_H
#include "point.h"

class Point;

class Vector_dir{
public:
    double x;
    double y;
    double z;

    Vector_dir();
    Vector_dir(double x, double y, double z);
    Vector_dir(const Vector_dir& vect);
    Vector_dir(const Point& A, const Point& B);
    Vector_dir(const Vector_dir& vect, double k);
    Vector_dir(const Vector_dir& vect1, const Vector_dir& vect2);

    void Set_vector(double, double, double);
    void Set_vector(const Vector_dir& vect, double k);

    void invers();
};

#endif // VECTOR_DIR_H
