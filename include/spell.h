#ifndef SPELL_H
#define SPELL_H
#include "basic_funcs.hpp"
#include "object.h"
#include "vector_dir.h"
#include "point.h"

class Spell
{
    public:
        Spell(Point, double, double, Vector_dir*);
        void Draw_spell();
        int Move_spell();
        int Get_damage();
        double Get_radius_act();

        int Check_spell_map(int***, int);
        int Check_spell_pl(Point);
        Point Get_location();

        virtual ~Spell();

    private:
        Object** spell_models;
        Point location;
        Vector_dir vect_move;
        int time_exist;
        int max_time_exist;
        int damage;
        double radius_act;
};

#endif // SPELL_H
